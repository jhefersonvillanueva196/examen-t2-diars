﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_T2_DIARS.Models
{
    public class Pokemon
    {
        public int idPokemon { get; set; }
        public string nomPokemon { get; set; }
        public string tipPokemon { get; set; }
        public string imgPokemon { get; set; }
    }
}
