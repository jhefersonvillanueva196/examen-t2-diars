﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Proyecto_T2_DIARS.Models;

namespace Proyecto_T2_DIARS.BDD.Mapping
{
    public class PokemonMap :IEntityTypeConfiguration<Pokemon>
    {
        public void Configure(EntityTypeBuilder<Pokemon> builder)
        {
            builder.ToTable("Pokemon");
            builder.HasKey(po => po.idPokemon);
        }
    }
}
