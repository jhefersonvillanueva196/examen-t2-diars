﻿using Microsoft.EntityFrameworkCore;
using Proyecto_T2_DIARS.BDD.Mapping;
using Proyecto_T2_DIARS.Models;


namespace Proyecto_T2_DIARS.BDD
{
    public class PokemonContext:DbContext
    {
        public DbSet<Pokemon> pokemones { get; set; }
        public DbSet<Usuario> usuarios { get; set; }
        public PokemonContext(DbContextOptions<PokemonContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PokemonMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
        }
    }
}
