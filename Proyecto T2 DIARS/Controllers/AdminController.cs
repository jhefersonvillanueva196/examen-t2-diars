﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System.Security.Claims;
using Proyecto_T2_DIARS.BDD;
using Proyecto_T2_DIARS.Models;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Proyecto_T2_DIARS.Controllers
{
    public class AdminController:Controller
    {
        private IWebHostEnvironment hosting;
        private PokemonContext context;

        public AdminController(PokemonContext context, IWebHostEnvironment hosting)
        {
            this.hosting = hosting;
            this.context = context;
        }

        

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Autores = context.pokemones.ToList();
            return View(new Pokemon());
        }

        [HttpPost]
        public IActionResult Create(Pokemon pokemon, List<IFormFile> files)
        {
            if (ModelState.IsValid)
            {
                pokemon.imgPokemon = SaveFile(files[0]);

                context.pokemones.Add(pokemon);
                context.SaveChanges();


                return RedirectToAction("Create");
            }

            ViewBag.Autores = context.pokemones.ToList();
            return View(pokemon);
        }

        private string SaveFile(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 && file.ContentType == "image/png")
            {
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }

    }
}

