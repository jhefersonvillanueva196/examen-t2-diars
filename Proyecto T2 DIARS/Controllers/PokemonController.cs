﻿using Microsoft.AspNetCore.Mvc;
using Proyecto_T2_DIARS.BDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_T2_DIARS.Controllers
{
    public class PokemonController:Controller
    {
        private PokemonContext context;
        public PokemonController(PokemonContext context)
        {
            this.context = context;
        }
        [HttpGet]
        public IActionResult MostrarPokemon()
        {
            var pokemon = context.pokemones;
            bool valor = true;
            if (pokemon == null)
                valor = false;
            else
                valor = true;
            Console.WriteLine(valor);
            return View("MostrarPokemon",pokemon);
        }
    }
}
